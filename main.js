const Game = require('./src/game.js');
const PercentAi = require('./src/ai/percent_ai.js');
const HumanAi = require('./src/ai/human_ai.js');
const Player = require('./src/player.js');
const Card = require('./src/card.js');
const EventManager = require('./src/event.js').EventManager;

// let cards = [
// 	new Card('Spider'),
// 	new Card('Spider'),
// 	new Card('Rock Fall'),
// 	new Card('Rock Fall'),
// 	new Card('Snakes'),
// 	new Card('Snakes'),
// 	new Card('Fire'),
// 	new Card('Fire'),

// 	new Card(Card.types.TREASURE, 7),
// 	new Card(Card.types.TREASURE, 7),
// 	new Card(Card.types.TREASURE, 11),
// 	new Card(Card.types.TREASURE, 11),
// 	new Card(Card.types.TREASURE, 13),
// 	new Card(Card.types.TREASURE, 13),
// 	new Card(Card.types.TREASURE, 17),
// 	new Card(Card.types.TREASURE, 17)
// ];

let cards = [
	new Card([Card.dangers.CAVE_COLLAPSE]),						// -

	new Card([Card.dangers.FIRE, Card.dangers.FIRE]),			// -
	new Card([Card.dangers.PITFALL, Card.dangers.PITFALL]),		// -
	new Card([Card.dangers.SNAKES, Card.dangers.SNAKES]),		// -
	new Card([Card.dangers.SNAKES, Card.dangers.SNAKES]),		// -
	new Card([Card.dangers.SPIDERS, Card.dangers.SPIDERS]),		// -
	new Card([Card.dangers.SPIDERS, Card.dangers.SPIDERS]),		// -

	new Card([Card.dangers.FIRE, Card.dangers.SNAKES]),			// -
	new Card([Card.dangers.FIRE, Card.dangers.SPIDERS]),		// -
	new Card([Card.dangers.FIRE, Card.dangers.PITFALL]),
	new Card([Card.dangers.PITFALL, Card.dangers.SNAKES]),		// -
	new Card([Card.dangers.PITFALL, Card.dangers.SPIDERS]),		// -
	new Card([Card.dangers.SNAKES, Card.dangers.SPIDERS]),		// -
	new Card([Card.dangers.SNAKES, Card.dangers.SPIDERS]),		// -

	new Card([Card.dangers.SNAKES], 7),							// -
	new Card([Card.dangers.SPIDERS], 7),						// -
	new Card([Card.dangers.SNAKES], 11),						// -
	new Card([Card.dangers.SPIDERS], 11),						// -
	new Card([Card.dangers.SNAKES], 15),
	new Card([Card.dangers.SPIDERS], 15),
	new Card([Card.dangers.SNAKES], 17),						// -
	new Card([Card.dangers.SPIDERS], 17),						// -
	new Card([Card.dangers.SNAKES], 19),						// -
	new Card([Card.dangers.SPIDERS], 19),						// -

	new Card([Card.dangers.FIRE], 13),							// -
	new Card([Card.dangers.PITFALL], 13),						// -
	new Card([Card.dangers.FIRE], 23),							// -
	new Card([Card.dangers.PITFALL], 23),						// -
	// 22, 18
	// 6

	// new Card('Double Wild'),
	// new Card('Flat 4 money Wild'),
];

function toPercent(num, places = 6) {
	return num.toFixed(places) + "%";
}

console.log('Deck Size: ' + cards.length);

let players = [
	new Player(new HumanAi(), 'Human Player'),
	new Player(new PercentAi(0.15), '15% AI'),
	new Player(new PercentAi(0.30), '30% AI'),
	new Player(new PercentAi(0.75), '50% AI'),
];

let event_mgr = new EventManager();

event_mgr.on('next_round', function (game) {
	console.log("\nRound " + game.curr_round);
	console.log('Players')
	console.log('===============');

	for (let i = 0; i < game.players.length; i++) {
		console.log(game.players[i].getName() +
			"\n\tbanked:" +
			game.players[i].getBankedPoints() +
			"\n\tin hand:" +
			game.players[i].getPoints()
		);
	}
});

event_mgr.on('end_game', function (game) {
	console.log('-- Game End --')
	console.log('Players')
	console.log('===============');

	for (let i = 0; i < game.players.length; i++) {
		console.log(game.players[i].getName() +
			"\n\tbanked:" +
			game.players[i].getBankedPoints() +
			"\n\tin hand:" +
			game.players[i].getPoints()
		);
	}
});

event_mgr.on('danger_round_end', function (game) {
	console.log('The players (' + game.players_in_round.map(p => p.getName()).join(', ') + ') ran into a ' + game.in_play[game.in_play.length - 1].getName());
});

event_mgr.on('retreat_round_end', function (game) {
	console.log('All the players retreated');
});

event_mgr.on('flipped_card', function (card) {
	console.log("\nFlipped a " + card.getName());
});

event_mgr.on('vote', function (votes) {
	// console.log('Danger Chance: ' + PercentAi.getDeathChance(game));

	let players = [...votes.deeper, ...votes.retreat].sort((a, b) => {
		const nameA = a.getName().toUpperCase();
		const nameB = b.getName().toUpperCase();

		if (nameA < nameB) {
			return -1;
		}
		if (nameA > nameB) {
			return 1;
		}

		// names must be equal
		return 0;
	});

	console.log("Death Chance:");
	for (const player of players) {
		console.log("  " + player.getName() + ": " + toPercent(player.deathChance(votes.game)));
	}

	console.log();

	if (votes.deeper.length !== 0) {
		console.log(votes.deeper.map(p => p.getName()).join(', ') + ' voted to go deeper');
	}
	if (votes.retreat.length !== 0) {
		console.log(votes.retreat.map(p => p.getName()).join(', ') + ' voted to retreat');
	}
});

/*
event_mgr.on('before_flip', function(game) {
	console.log('Danger Chance: '+PercentAi.getDeathChance(game));
});
*/

let game = new Game(cards, players, event_mgr, 4);

console.log('starting game');
game.start();
