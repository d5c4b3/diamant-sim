class EventManager {
	listeners = {};


	emit(name, data) {
		if (!this.listeners.hasOwnProperty(name)) {
			return;
		}

		for (const callback of this.listeners[name]) {
			callback(data);
		}
	}

	on(name, callback) {
		if (!this.listeners.hasOwnProperty(name)) {
			this.listeners[name] = [];
		}

		this.listeners[name].push(callback);
	}
}

module.exports.EventManager = EventManager;
