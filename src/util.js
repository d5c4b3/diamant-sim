module.exports.shuffle = function(array) {
	for (let i = array.length - 1; i > 0; i--) {
		const r = Math.floor(Math.random() * (i+1));
		let array_i = array[i];
		array[i] = array[r];
		array[r] = array_i;
	}
}
