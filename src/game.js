const Util = require('./util.js');
const Card = require('./card.js');
const Upgrades = require('./upgrades/upgrades.js');
const { EventManager } = require('./event.js');
const { FIRE_INSURANCE_FLAT } = require('./upgrades/upgrades.js');

class Game {
	reshuffle_first_round_collapse = false;

	/** @type Card[] */
	cards;

	/** @type Player[] */
	players;

	/** @type EventManager */
	event_mgr;

	/** @type Card[] */
	deck = [];

	/** @type Card[] */
	in_play = [];

	/** @type Player[] */
	players_in_round = [];

	/**
	 * 
	 * @param {Card[]} cards 
	 * @param {Player[]} players 
	 * @param {EventManager} event_mgr 
	 * @param {number} num_rounds 
	 */
	constructor(cards, players, event_mgr, num_rounds = 4) {
		// cards and players should remain static throughout the game
		// these exist so we can reinitialize things like
		// this.deck and this.players_in_round
		this.cards = cards;
		this.players = players;

		this.num_rounds = num_rounds;
		this.curr_round = 0;

		this.event_mgr = event_mgr;
	}

	resetRound() {
		this.curr_round++;

		// create new array with all the cards in it
		// if we did direct assignment everytime we drew a card
		// the original array would be changed. We don't want that.
		this.deck = [...this.cards];
		this.in_play = [];

		// create new array with all the players in it
		this.players_in_round = [...this.players];

		this.extra_treasure = 0;

		this.shuffleDeck();
	}

	start() {
		this.resetRound();
		this.event_mgr.emit('start_game', this);
		while (this.curr_round < this.num_rounds + 1) {
			this.doRound();
			this.resetRound();
		}
		this.event_mgr.emit('end_game', this);
	}

	doRound() {
		this.event_mgr.emit('next_round', this);

		// This is effectively a while(true), but with a built
		// in fail-safe in case I screw something up
		for (let i = 0; i < this.cards.length; i++) {
			this.event_mgr.emit('before_flip', this);

			let card = this.flipCard();

			// If the first card flipped is the CAVE_COLLAPSE then reset everything.
			// TODO: integrate this better with event system
			if (i === 0 && card.isDanger() && card.getDangers().includes(Card.dangers.CAVE_COLLAPSE) && this.reshuffle_first_round_collapse) {
				i--;
				continue;
			}

			if (this.players_in_round.length === 0) {
				this.event_mgr.emit('danger_round_end', this);
				break;
			}

			this.doVote();

			// Check for and apply FULL_PARTY_BONUS upgrades
			if (this.players.length === this.players_in_round.length) {
				for (const player of this.players_in_round) {
					if (player.hasUpgrade(Upgrades.FULL_PARTY_BONUS)) {
						player.addPoints(Upgrades.FULL_PARTY_BONUS.getValue());
					}
				}
			}

			// Check for and apply LONE_WOLF_BONUS upgrades
			if (this.players_in_round.length === 1 && this.players_in_round[0].hasUpgrade(Upgrades.LONE_WOLF_BONUS)) {
				this.players_in_round[0].addPoints(Upgrades.LONE_WOLF_BONUS.getValue());
			}

			if (this.players_in_round.length === 0) {
				this.event_mgr.emit('retreat_round_end', this);
				break;
			}
		}
	}

	flipCard() {
		// Flip a card from the deck
		let card = this.deck.pop();

		if (card === undefined) throw new Error('Cannot flip card in an empty deck');

		// Put the card in play
		this.in_play.push(card);

		this.applyDangerBonusUpgrades(card);

		// See what players died if any
		const dangers = this.getDangersInPlay();
		let alive = [];
		for (const player of this.players_in_round) {
			if (player.shouldDie(dangers)) {

				// Check and apply upgrades
				let points_to_bank = 0;
				if (dangers.includes(Card.dangers.FIRE)) {
					let half_insurance = 0;
					if (player.hasUpgrade(Upgrades.FIRE_INSURANCE_HALF)) {
						half_insurance = player.getPoints() / 2;
					}

					let flat_insurance = 0;
					if (player.hasUpgrade(FIRE_INSURANCE_FLAT)) {
						flat_insurance = Upgrades.FIRE_INSURANCE_FLAT.getValue()
					}

					// Doing it this way lets me handle the case that a player has both
					// flat and half insurance for fire. They'll get whichever is bigger, but not both.
					points_to_bank = Math.max(half_insurance, flat_insurance);
				}


				player.clearPoints();

				if (points_to_bank > 0) {
					player.addPoints(points_to_bank);
					player.bankPoints();
				}
			} else {
				alive.push(player);
			}
		}
		this.players_in_round = alive;

		// if the card is a treasure we need to divvy up points
		if (card.isTreasure()) {
			let extra = card.getTreasureValue() % this.players_in_round.length;
			this.extra_treasure += extra;


			let points_per_player = Math.floor(card.getTreasureValue() / this.players_in_round.length);

			for (const player of this.players_in_round) {

				// check for and apply upgrades
				if (card.getTreasureValue() <= Upgrades.SMALL_TREASURE_BONUS.getThreshold() && player.hasUpgrade(Upgrades.SMALL_TREASURE_BONUS)) {
					points += Upgrades.SMALL_TREASURE_BONUS.getValue();
				}

				if (card.getTreasureValue() >= Upgrades.BIG_TREASURE_BONUS.getThreshold() && player.hasUpgrade(Upgrades.BIG_TREASURE_BONUS)) {
					points += Upgrades.BIG_TREASURE_BONUS.getValue();
				}

				player.addPoints(points_per_player);
			}
		}

		this.event_mgr.emit('flipped_card', card);

		return card;
	}

	doVote() {
		let deeper = [];
		let retreat = [];
		for (const player of this.players_in_round) {
			// TODO: we should really create a read-only game_state
			// object that gets passed in, but that's a lot of work
			let vote = player.vote(this);
			if (vote) {
				deeper.push(player);
			} else {
				retreat.push(player);
			}
		}

		this.players_in_round = deeper;

		// If no one retreated, return. We don't have any logic 
		// that needs to be ran for the deeper players
		if (retreat.length === 0) {
			this.event_mgr.emit('vote', { deeper: deeper, retreat: retreat, game: this });
			return;
		}

		let points_per_player = Math.floor(this.extra_treasure / retreat.length);
		this.extra_treasure = this.extra_treasure % retreat.length;
		for (const player of retreat) {

			// Check for and apply upgrades
			let points = points_per_player;
			if (points_per_player <= Upgrades.SMALL_EXTRAS_BONUS.getThreshold() && player.hasUpgrade(Upgrades.SMALL_EXTRAS_BONUS)) {
				points += Upgrades.SMALL_EXTRAS_BONUS.getValue();
			}

			if (points_per_player >= Upgrades.BIG_EXTRAS_BONUS.getThreshold() && player.hasUpgrade(Upgrades.BIG_EXTRAS_BONUS)) {
				points += Upgrades.BIG_EXTRAS_BONUS.getValue();
			}

			player.addPoints(points);
			player.bankPoints();
		}

		this.event_mgr.emit('vote', { deeper: deeper, retreat: retreat, game: this });
	}

	shuffleDeck() {
		Util.shuffle(this.deck);
	}

	getDangersInPlay() {
		let dangers = [];
		for (const card of this.in_play) {
			if (card.isDanger()) {
				dangers.push(...card.getDangers());
			}
		}

		return dangers;
	}

	/**
	 * @param {Card} card 
	 */
	applyDangerBonusUpgrades(card) {
		if (!card.isDanger()) return;

		// Map dangers to their relevant upgrade so the 
		// following code gets a lot easier
		let danger_map = [
			{
				danger: Card.dangers.FIRE,
				upgrade: Upgrades.FIRE_BONUS,
			},
			{
				danger: Card.dangers.PITFALL,
				upgrade: Upgrades.PITFALL_BONUS,
			},
			{
				danger: Card.dangers.SNAKES,
				upgrade: Upgrades.SNAKE_BONUS,
			},
			{
				danger: Card.dangers.SPIDERS,
				upgrade: Upgrades.SPIDER_BONUS,
			},
		];

		// For each danger type check if the card has that danger
		// and if the player has that upgrade
		// if so give the player their bonus points
		for (const d of danger_map) {
			if (!card.includes(d.danger)) continue;
			if (!player.hasUpgrade(d.upgrade)) continue;

			player.addPoints(d.upgrade.getValue());
		}
	}

}

module.exports = Game;
