const Card = require('./card.js');
const Upgrades = require('./upgrades/upgrades.js');

class Player {
	points = 0;
	bank = 0;

	ai = null;
	name = '';

	upgrades = [];

	fire_resistance = 3;
	pitfall_resistance = 3;
	snake_resistance = 5;
	spider_resistance = 5;
	cave_collapse_resistance = 1;

	constructor(ai, name) {
		this.ai = ai;
		this.name = name;
	}

	getName() {
		return this.name;
	}

	getBankedPoints() {
		return this.bank;
	}

	bankPoints() {
		this.bank += this.points;
		this.points = 0;
	}

	getPoints() {
		return this.points;
	}

	addPoints(points) {
		this.points += points;
	}

	clearPoints() {
		this.points = 0;
	}

	buyUpgrade(upgrade) {
		this.upgrades.push(upgrade);

		switch (upgrade) {
			case Upgrades.RESISTANCE_FIRE:
				this.fire_resistance += 1;
				break;
			case Upgrades.RESISTANCE_PITFALLS:
				this.pitfall_resistance += 1;
				break;
			case Upgrades.RESISTANCE_SNAKES:
				this.snake_resistance += 1;
				break;
			case Upgrades.RESISTANCE_SPIDERS:
				this.spider_resistance += 1;
				break;
		}

	}

	hasUpgrade(upgrade) {
		return this.upgrades.includes(upgrade);
	}

	deathChance(game) {
		let dangers = game.getDangersInPlay();

		// look through deck for cards that match danger cards in play
		let num_death_cards = 0;
		for (const card of game.deck) {
			if (!card.isDanger()) continue;

			let after_flip_dangers = [...dangers];
			after_flip_dangers.push(...card.getDangers());

			const num_fires = after_flip_dangers.filter(d => d === Card.dangers.FIRE).length;
			const num_pitfalls = after_flip_dangers.filter(d => d === Card.dangers.PITFALL).length;
			const num_spiders = after_flip_dangers.filter(d => d === Card.dangers.SPIDERS).length;
			const num_snakes = after_flip_dangers.filter(d => d === Card.dangers.SNAKES).length;
			const num_cave_collapse = after_flip_dangers.filter(d => d === Card.dangers.CAVE_COLLAPSE).length;

			if (num_fires >= this.fire_resistance
				|| num_pitfalls >= this.pitfall_resistance
				|| num_spiders >= this.spider_resistance
				|| num_snakes >= this.snake_resistance
				|| num_cave_collapse >= this.cave_collapse_resistance
			) {
				num_death_cards += 1;
			}
		}

		// calculate death chance
		return num_death_cards / game.deck.length;
	}

	shouldDie(dangers) {
		const num_fires = dangers.filter(d => d === Card.dangers.FIRE).length;
		const num_pitfalls = dangers.filter(d => d === Card.dangers.PITFALL).length;
		const num_spiders = dangers.filter(d => d === Card.dangers.SPIDERS).length;
		const num_snakes = dangers.filter(d => d === Card.dangers.SNAKES).length;
		const num_cave_collapse = dangers.filter(d => d === Card.dangers.CAVE_COLLAPSE).length;

		if (num_fires >= this.fire_resistance
			|| num_pitfalls >= this.pitfall_resistance
			|| num_spiders >= this.spider_resistance
			|| num_snakes >= this.snake_resistance
			|| num_cave_collapse >= this.cave_collapse_resistance
		) {
			return true;
		}

		return false;
	}

	/**
	 * Returns true for dig deeper and false for retreat
	 */
	vote(game_state) {
		return this.ai.vote(game_state);
	}
}

module.exports = Player;
