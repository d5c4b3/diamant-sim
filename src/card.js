class Card {

	// These should be treated as read-only
	dangers = [];
	value = null;

	constructor(dangers = null, value = null) {
		// if (type !== Card.types.DANGER && type !== Card.types.TREASURE && type !== Card.types.BOTH) {
		// 	throw new RangeError('Type must be a valid card type');
		// }

		// if ((type === Card.types.TREASURE || type === Card.types.BOTH) && value === null) {
		// 	throw new RangeError('Treasure cards must have a defined value');
		// }

		// if ((type === Card.types.TREASURE || type === Card.types.BOTH) && (!Number.isInteger(value) || value < 1)) {
		// 	throw new RangeError('value must be greater than 0');
		// }

		this.dangers = dangers || [];
		this.value = value;
	}

	isDanger() {
		//return (this.type === Card.types.DANGER || this.type === Card.types.BOTH);
		return this.dangers.length > 0;
	}

	isTreasure() {
		// return (this.type === Card.types.TREASURE || this.type === Card.types.BOTH);
		return this.value !== null;
	}

	getDangers() {
		return this.dangers;
	}

	getTreasureValue() {
		return this.value;
	}

	getName() {
		let str = "";

		if (this.isDanger()) {
			str = this.dangers.map(d => Card.dangerNames[d]).join(' ');
		} else {
			str = 'Treasure';
		}

		if (this.isTreasure()) {
			str += ' ' + this.getTreasureValue();
		}

		return str;
	}
}

Card.dangerNames = [
	'Fire',
	'Pitfall',
	'Snakes',
	'Spiders',
	'Wild',
	'Cave Collapse',
]

Card.dangers = {
	FIRE: 0,
	PITFALL: 1,
	SNAKES: 2,
	SPIDERS: 3,
	WILD: 4,
	CAVE_COLLAPSE: 5,
}

module.exports = Card;
