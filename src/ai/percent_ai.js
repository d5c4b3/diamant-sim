const Ai = require('./ai.js');
const Card = require('../card.js');

class PercentAi extends Ai {

	constructor(cutoff) {
		super();
		if (cutoff < 0 || cutoff > 1) {
			throw new RangeError('cutoff must be between 0 and 1');
		}

		this.cutoff = cutoff;
	}

	static getDeathChance(game_state) {
		// Chance of death is
		// num cards that kill us / cards left in deck

		// get danger cards in play
		// let death_values = [];
		// for (const card of game_state.in_play) {
		// 	if (card.isDanger()) {
		// 		death_values.push(...card.getDangers());
		// 	}
		// }

		let dangers = game_state.getDangersInPlay();

		// look through deck for cards that match danger cards in play
		let num_death_cards = 0;
		for (const card of game_state.deck) {
			if (!card.isDanger()) continue;

			let after_flip_dangers = [...dangers];
			after_flip_dangers.push(...card.getDangers());

			const num_fires = after_flip_dangers.filter(d => d === Card.dangers.FIRE).length;
			const num_pitfalls = after_flip_dangers.filter(d => d === Card.dangers.PITFALL).length;
			const num_spiders = after_flip_dangers.filter(d => d === Card.dangers.SPIDERS).length;
			const num_snakes = after_flip_dangers.filter(d => d === Card.dangers.SNAKES).length;
			const num_cave_collapse = after_flip_dangers.filter(d => d === Card.dangers.CAVE_COLLAPSE).length;

			if (num_fires >= 3 || num_pitfalls >= 3 || num_spiders >= 5 || num_snakes >= 5 || num_cave_collapse >= 1) {
				num_death_cards += 1;
			}
		}

		// calculate death chance
		return num_death_cards / game_state.deck.length;
	}

	vote(game_state) {
		let death_chance = PercentAi.getDeathChance(game_state);
		if (death_chance >= this.cutoff) {
			return false;
		}

		return true;
	}
}

module.exports = PercentAi;
