const Ai = require('./ai.js');

class CachedAiDecorator extends Ai {
	cache = {};

	construct(ai) {
		this.ai = ai;
	}

	vote(game_state) {
		let cache_key = "round "+game_state.curr_round;
		if (cache.hasOwnProperty(cache_key)) {
			return cache[cache_key];
		}

		let vote = this.ai.vote(game_state);

		cache[cache_key] = vote;
		return vote;
	}
}

module.exports = CachedAiDecorator;
