const Ai = require('./ai.js');
const prompt = require('../prompt-sync.js')();

class HumanAi extends Ai {
	vote(game_state) {
		let ans = null;

		// As long as we don't have an answer try to get an answer
		while(ans === null) {
			// Ask if they want to continue
			ans = prompt('Would you like to dig deeper? (y/n): ');
			ans = ans.toLowerCase();

			// they do, return true
			if (ans === 'y' || ans === 'yes') {
				return true;
			}

			// the don't, return false
			if (ans === 'n' || ans === 'no') {
				return false;
			}

			// They didn't give us a valid answer
			// keep asking till they do
			ans = null;
		}
	}
}

module.exports = HumanAi;
