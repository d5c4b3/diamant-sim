const Upgrade = require('./upgrade');

class ValueUpgrade extends Upgrade {
    constructor(name, value, description) {
        super(name, description);

        this.value = value;
    }

    getValue() {
        return this.value;
    }
}

modules.export = ValueUpgrade;