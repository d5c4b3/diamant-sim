const Upgrade = require('./upgrade');
const ValueUpgrade = require('./value_upgrade');
const ThresholdUpgrade = require('./threshold_upgrade');

class Upgrades { }

Upgrades.RESISTANCE_FIRE = new Upgrade('Fire Resistance', 'Increases the number of fire hazards required to kill you');
Upgrades.RESISTANCE_PITFALLS = new Upgrade('Pitfall Resistance', 'Increases the number of pitfall hazards required to kill you');
Upgrades.RESISTANCE_SNAKES = new Upgrade('Snake Resistance', 'Increases the number of snake hazards required to kill you');
Upgrades.RESISTANCE_SPIDERS = new Upgrade('Spider Resistance', 'Increases the number of spider hazards required to kill you');
Upgrades.RESISTANCE_CAVE_COLLAPSE = new Upgrade('Cave Collapse Immunity', 'Increases the number of cave collapse hazards required to kill you');

Upgrades.SMALL_EXTRAS_BONUS = new ThresholdUpgrade('Small Extras Bonus', 1, 2, 'You get a bonus when you retreat for a small amount of extra treasure');
Upgrades.BIG_EXTRAS_BONUS = new ThresholdUpgrade('Big Extras Bonus', 1, 10, 'You get a bonus when you retreat for a large amount of extra treasure');

Upgrades.SMALL_TREASURE_BONUS = new ThresholdUpgrade('Small Treasure Bonus', 1, 3, 'You get a bonus when a small treasure card is encountered');
Upgrades.BIG_TREASURE_BONUS = new ThresholdUpgrade('Big Treasure Bonus', 1, 17, 'You get a bonus when a large treasure card is encountered');

Upgrades.FIRE_INSURANCE_FLAT = new ValueUpgrade('Fire Insurance (Flat)', 3, 'You retain a small amount of your money when you die to fire');
Upgrades.FIRE_INSURANCE_HALF = new Upgrade('Fire Insurance (Half)', 'You retain half your money when you die to fire');

Upgrades.FULL_PARTY_BONUS = new ValueUpgrade('Full Party Bonus', 2, 'You get a small bonus if all players delve deeper');
Upgrades.LONE_WOLF_BONUS = new ValueUpgrade('Lone Wolf Bonus', 2, 'You get a small bonus if you are the only player to delve deeper');

Upgrades.FIRE_BONUS = new ValueUpgrade('Burn Bonus', 1, 'You get a bonus when a fire hazard is encountered');
Upgrades.PITFALL_BONUS = new ValueUpgrade('Fall Bonus', 1, 'You get a bonus when a pitfall hazard is encountered');
Upgrades.SNAKE_BONUS = new ValueUpgrade('Snake Bonus', 1, 'You get a bonus when a snake hazard is encountered');
Upgrades.SPIDER_BONUS = new ValueUpgrade('Spider Bonus', 1, 'You get a bonus when a spider hazard is encountered');

// I don't remember what this one was supposed to do
//Upgrades.DEATH_EXTRAS = new Upgrade('Death Extras', '');

Upgrades.GRAVE_ROBBER = new Upgrade('Grave Robber', 'When a player dies you gain a small amount of their treasure');



module.exports = Upgrades;