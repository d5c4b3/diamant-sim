const ValueUpgrade = require('./value_upgrade');

class ThresholdUpgrade extends ValueUpgrade {
    constructor(name, value, threshold, description) {
        super(name, value, description);

        this.threshold = threshold;
    }

    getThreshold() {
        return this.threshold;
    }
}

modules.export = ThresholdUpgrade;