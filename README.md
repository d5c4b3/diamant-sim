A simulator for the game Diamant. Most of the scripts are configured to use a shortened 16 card version of the game.

# Setup
This setup is a guide for setting up a really simple developer environment. This guide will get you Node 14, a text editor, and the code from this repository.

## Installing git
This repository is hosted on a website that uses `git` to manage code and allow collaboration. To install git on Windows go to https://git-scm.com/download/win, download the installer, and run the it. Press "next" on the license screen, select "use bundled OpenSSH, then press Install.

The specific program that we are using here will be called "Git Bash" which I may refer to as "terminal" during portions of the setup.

## Installing NVM
Node Version Manager is a program that lets you manage which versions of NodeJS are installed on your machine.

Go to https://github.com/coreybutler/nvm-windows/releases and download the `nvm-setup.zip` file. Extract the setup file and run it. Once NVM is installed search "Git Bash" in the start menu, right click and select "Run as administrator". NVM needs admin access to set the node version, but once it's set we'll go back to using a normal terminal. 

Verify that NVM is installed by running
```sh
nvm --version
```

You should see something like this:
![image.png](https://res.cloudinary.com/practicaldev/image/fetch/s--hAH3_fBd--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://cdn.hashnode.com/res/hashnode/image/upload/v1599509445634/CwrXoWsAL.png)

Now run 
```sh
nvm install 14.18.2
```
followed by 
```sh
nvm use 14.18.2
```

Verify that you now have node installed by running
```sh
node --version
```

**IMPORTANT** exit out of Git Bash and re-open it like normal. We DO NOT want an admin terminal any more.

## Notepad++
Notepad++ is a text editing software that will make your life much easier by providing you with syntax highlighting, monospaced font, and a number of other really useful features. Install the newest version from here https://notepad-plus-plus.org/downloads

## Getting this code
Open a Git Bash terminal and run the following. This will **C**hange **D**irectory into the Documents folder, then clone this project into a new folder, then change directory into the new project folder. 
```sh
cd Documents
git clone https://gitlab.com/d5c4b3/diamant-sim.git
cd diamant-sim
```

If everything worked you should be able to run
```sh
node main.js
```
to play a basic command line version of the game. 

## Editing code
Open Notepad++, select File > Open Folder as Workspace..., then navigate to the diamant-sim, and press Select Folder. This will open the diamant-sim project in a side bar so you can easily open files in the project.

# Playing the game
To play the game against randomly generated selected AI run
```sh
node main.js
```

# Adjusting to your own parameters
The `all_ai.js` script can be used as an example of a single game simulation. It plays out a single game with all AI players. The game is displayed out to the console for review.

The `all\_percentages.js` script shows how you can run multiple thousands of simulations for large amounts of data. Rather than displaying everything, stats are gathered and aggregated at the end. It's also possible to adjust game parameters (such as player AI and starting deck) for each game to collect differences in starting conditions.

# AI
## PercentAi
The PercentAi cuts its losses soley based on the chance that the next flipped card ends the round. A 10% AI will retreat as soon as the "death chance" is 10% or greater. So higher percentage AIs play riskier and lower percentage AIs more conservatively.

# Simulations
## all\_percentages.js
This script runs 1000 game simulations for the PercentAi for each decimal percent. Each game is played with 4 matching PercentAi. This data is then output to the terminal in CSV format.

The `cards` array let's you configure the starting deck.    
`simulations_per_value` deterimines how many games are ran for each decimal percent.    
`rounds_per_game` adjusts how many rounds each game lasts for.    

Usage:
```
node all_percentages.js > all_percentages.csv
```

It seems that (atleast when all players are the same) a cutoff of ~17% is optimal.

## all\_ai.js
This isn't so much a simulation. This script simply outputs a single game played with all AI. The game is logged verbosely and can be used for debugging after game logic has been updated.


# TODO: 
AI that retreat after X card flips
AI that retreats only if it has more than X treasure
Weighted combo AI that adjusts risk evaluation as treasure goes up
AI that retreats after leftover threshold reaches a particular number
AI that always plays two more rounds than everyone else
