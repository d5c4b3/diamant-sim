const Game = require('./src/game.js');
const PercentAi = require('./src/ai/percent_ai.js');
const Player = require('./src/player.js');
const Card = require('./src/card.js');
const EventManager = require('./src/event.js').EventManager;

let cards = [
	new Card(Card.types.DANGER, 'Spider'),
	new Card(Card.types.DANGER, 'Spider'),
	new Card(Card.types.DANGER, 'Rock Fall'),
	new Card(Card.types.DANGER, 'Rock Fall'),
	new Card(Card.types.DANGER, 'Snakes'),
	new Card(Card.types.DANGER, 'Snakes'),
	new Card(Card.types.DANGER, 'Fire'),
	new Card(Card.types.DANGER, 'Fire'),
	
	new Card(Card.types.TREASURE, 7),
	new Card(Card.types.TREASURE, 7),
	new Card(Card.types.TREASURE, 11),
	new Card(Card.types.TREASURE, 11),
	new Card(Card.types.TREASURE, 13),
	new Card(Card.types.TREASURE, 13),
	new Card(Card.types.TREASURE, 17),
	new Card(Card.types.TREASURE, 17)
];

let players = [
	new Player(new PercentAi(0.15), '15% AI'),
	new Player(new PercentAi(0.30), '30% AI'),
	new Player(new PercentAi(0.55), '55% AI'),
	new Player(new PercentAi(0.75), '75% AI'),
];

let event_mgr = new EventManager();

event_mgr.on('next_round', function(game) {
	console.log("\nRound "+game.curr_round);
	console.log('Players')
	console.log('===============');

	for (let i = 0; i < game.players.length; i++) {
		console.log(game.players[i].getName()+
			"\n\tbanked:"+
			game.players[i].getBankedPoints()+
			"\n\tin hand:"+
			game.players[i].getPoints()
		);
	}
});

event_mgr.on('end_game', function(game) {
	console.log('-- Game End --')
	console.log('Players')
	console.log('===============');

	for (let i = 0; i < game.players.length; i++) {
		console.log(game.players[i].getName()+
			"\n\tbanked:"+
			game.players[i].getBankedPoints()+
			"\n\tin hand:"+
			game.players[i].getPoints()
		);
	}
});

event_mgr.on('danger_round_end', function(game) {
	console.log('The players ('+game.players_in_round.map(p => p.getName()).join(', ')+') ran into a '+game.in_play[game.in_play.length-1].value);
});

event_mgr.on('retreat_round_end', function(game) {
	console.log('All the players retreated');
});

event_mgr.on('flipped_card', function(card) {
	console.log("\nFlipped a "+card.value);
});

event_mgr.on('vote', function(votes) {
	console.log('Danger Chance: '+PercentAi.getDeathChance(game));
	if (votes.deeper.length !== 0) {
		console.log(votes.deeper.map(p => p.getName()).join(', ')+' voted to go deeper');
	}
	if (votes.retreat.length !== 0) {
		console.log(votes.retreat.map(p => p.getName()).join(', ')+' voted to retreat');
	}
});

/*
event_mgr.on('before_flip', function(game) {
	console.log('Danger Chance: '+PercentAi.getDeathChance(game));
});
*/

let game = new Game(cards, players, event_mgr, 4);

console.log('starting game');
game.start();
