const Game = require('./src/game.js');
const PercentAi = require('./src/ai/percent_ai.js');
const Player = require('./src/player.js');
const Card = require('./src/card.js');
const EventManager = require('./src/event.js').EventManager;

let cards = [
	new Card([Card.dangers.CAVE_COLLAPSE]),						// -

	new Card([Card.dangers.FIRE, Card.dangers.FIRE]),			// -
	new Card([Card.dangers.PITFALL, Card.dangers.PITFALL]),		// -
	new Card([Card.dangers.SNAKES, Card.dangers.SNAKES]),		// -
	new Card([Card.dangers.SNAKES, Card.dangers.SNAKES]),		// -
	new Card([Card.dangers.SPIDERS, Card.dangers.SPIDERS]),		// -
	new Card([Card.dangers.SPIDERS, Card.dangers.SPIDERS]),		// -

	new Card([Card.dangers.FIRE, Card.dangers.SNAKES]),			// -
	new Card([Card.dangers.FIRE, Card.dangers.SPIDERS]),		// -
	new Card([Card.dangers.FIRE, Card.dangers.PITFALL]),
	new Card([Card.dangers.PITFALL, Card.dangers.SNAKES]),		// -
	new Card([Card.dangers.PITFALL, Card.dangers.SPIDERS]),		// -
	new Card([Card.dangers.SNAKES, Card.dangers.SPIDERS]),		// -
	new Card([Card.dangers.SNAKES, Card.dangers.SPIDERS]),		// -

	new Card([Card.dangers.SNAKES], 1),							// -
	new Card([Card.dangers.SPIDERS], 1),						// -
	new Card([Card.dangers.SNAKES], 3),						// -
	new Card([Card.dangers.SPIDERS], 3),						// -
	new Card([Card.dangers.SNAKES], 5),
	new Card([Card.dangers.SPIDERS], 5),
	new Card([Card.dangers.SNAKES], 17),						// -
	new Card([Card.dangers.SPIDERS], 17),						// -
	new Card([Card.dangers.SNAKES], 90),						// -
	new Card([Card.dangers.SPIDERS], 90),						// -

	new Card([Card.dangers.FIRE], 90),							// -
	new Card([Card.dangers.PITFALL], 90),						// -
	new Card([Card.dangers.FIRE], 90),							// -
	new Card([Card.dangers.PITFALL], 90),						// -
	// 22, 18
	// 6

	// new Card('Double Wild'),
	// new Card('Flat 4 money Wild'),
];

const event_mgr = new EventManager();

let points = 0;
let retreats = 0;
let deaths = 0;
let depth = 1;

event_mgr.on('danger_round_end', function (game) {
	deaths++;
});

event_mgr.on('retreat_round_end', function (game) {
	retreats++;
	depth--;
});

event_mgr.on('vote', function (votes) {
	depth++;
});

const simulations_per_value = 1000;
const rounds_per_game = 4;

const stats = [];

for (let i = 1; i <= 100; i++) {
	ai_cutoff = i / 100;

	process.stderr.write("ai_cutoff: " + ai_cutoff + "\r");

	// Reset staticstics
	points = 0;
	retreats = 0;
	deaths = 0;
	depth = 1;

	// Run simulations
	for (let sim_num = 1; sim_num <= simulations_per_value; sim_num++) {

		const players = [
			new Player(new PercentAi(ai_cutoff), 'Player 1'),
			new Player(new PercentAi(ai_cutoff), 'Player 2'),
			new Player(new PercentAi(ai_cutoff), 'Player 3'),
			new Player(new PercentAi(ai_cutoff), 'Player 4'),
		];

		const game = new Game(cards, players, event_mgr, rounds_per_game);
		game.start();

		points += players[0].getBankedPoints();
	}

	// Record statistics
	const row = {
		ai_cutoff: ai_cutoff,
		points: points,
		retreats: retreats,
		deaths: deaths,
		cards_flipped: depth,
	};

	stats.push(row);
}


// TODO: write this to file rather than output
console.log("sep=,");
console.log("ai_cutoff,points,retreats,deaths,cards_flipped,point_avg,retreat_avg,death_avg,cards_flipped_avg");
for (row of stats) {
	console.log(`${row.ai_cutoff},${row.points},${row.retreats},${row.deaths},${row.cards_flipped},${row.points / simulations_per_value},${row.retreats / simulations_per_value},${row.deaths / simulations_per_value},${row.cards_flipped / simulations_per_value}`);
}
